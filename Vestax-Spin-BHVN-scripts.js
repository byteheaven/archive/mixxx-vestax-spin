// Script file for BYTE HEAVEN Mixxx Vestax Spin 2 mapping
// For Mixxx v2
// Tom Surace, 2013-2017
// Bill Good, Oct 31, 2010
// Anders Gunnarsson ???
// Others..?

var Spin = {};
(function(Spin) {
    
//
// **** Configuration
//

// Scratch alpha/beta.
// It's a small wheel, but with quite a bit of momentum and good resolution,
// so I like it to be pretty sensitive.
//
// 0 < alpha < 1
// 0 < beta <= 2 (stable only if beta < 1)

var SCRATCH_ALPHA = 0.3;   // default = 1/8 = 0.125
var SCRATCH_BETA = 0.005;  // default == alpha / 32 == 0.004
var SCRATCH_RAMP = false;   // ramp on release?

// I honestly can not decide whether I like soft takeover on the
// mid and/or filter controls.

var MID_SOFT_TAKEOVER = false;
var FILTER_SOFT_TAKEOVER = false;

//
// **** Internal constants
//

// Loghts to kill on both channels 0 and 1
var DECK_LIGHTS = [
    0x32, 0x35, 0x33, 0x24, 0x25, 0x46, 0x42, 0x21, 0x20, // lights
    0x29, 0x2a, 0x2b, 0x2c, 0x2d // VU meters
];
    
// Lights to kill on channel 2 only
var MISC_LIGHTS = [
    0x26, // automix
    0x29,
    0x28,
    0x2a  // wheels
];

var BUTTON_RELEASED = 0x00;
var BUTTON_PRESSED = 0x7F;

var LIGHT_ON = 0x7f;
var LIGHT_OFF = 0x00;

/**
 * @constructor
 * Spin has really simple on/off lights that all work the same.
 * That's actually a nice change of pace.
 *
 * @param channel {number} 0-indexed channel, 0-2
 * @param control {number} Midi controller to turn light on/off.
 * @param 
 */
function LightController(channel, control) {
    this.channel = channel;
    this.control = control;
    this.state = LIGHT_OFF;
}

LightController.turnOn = function(channel, control) {
    midi.sendShortMsg(0x90 + channel, control, LIGHT_ON);
};

LightController.turnOff = function(channel, control) {
    midi.sendShortMsg(0x90 + channel, control, LIGHT_OFF);
};

LightController.prototype.on = function() {
    LightController.turnOn(this.channel, this.control);
    this.state = LIGHT_ON;
};

LightController.prototype.off = function() {
    LightController.turnOff(this.channel, this.control);
    this.state = LIGHT_OFF;
};

/**
 * @constructor
 * Only things that are common to both decks
 *
 * @param {number} channel 0 or 1, for left or right deck
 */
function Deck(channel) {
    // Channel
    this.channel = channel;
    
    // Deck num and group name variables, cached for performance
    this.deckNum = this.channel + 1;
    this.groupName = '[Channel' + this.deckNum + ']';
    this.effectRackName = '[QuickEffectRack1_' + this.groupName + ']';
    this.effectRackEffect1Name = '[QuickEffectRack1_' + this.groupName + '_Effect1]';
    
    /** @type {boolean} */    
    this.isFx = false;
    
    /** @type {boolean} */
    this.isVinylMode = false;

    this.lights = {};
    this.lights.fx = new LightController(channel, 0x42);
    this.lights.vinyl = new LightController(channel, 0x24);
}

/**
 * Handle the FX toggle button.
 */
Deck.prototype.handleFx = function(value) {
    if (value !== BUTTON_PRESSED) {
        return;
    }
    
    this.isFx = ! this.isFx;
    
    if (this.isFx) {
        if (MID_SOFT_TAKEOVER) {
            engine.softTakeoverIgnoreNextValue(this.groupName, "filterMid");
        }
        this.lights.fx.on();
    }
    else {
        // Immediately reset the super control
        engine.setValue(this.effectRackName, "super1", 0.5);
        
        // make sure soft takeover will work when we grab it back from 0
        if (FILTER_SOFT_TAKEOVER) {
            engine.softTakeoverIgnoreNextValue(this.effectRackName, "super1");
        }
        this.lights.fx.off(); // switch to mid control
    }
};

/**
 * Handle mid control.
 *
 * @param {number} value incoming MIDI control value
 */
Deck.prototype.handleMid = function(value) {
    // In FX mode, Mid is the filter control (or whatever
    // the user has mapped to the special "first effect rack")
    // In non-FX mode, it's just a mid control.
    
    
    if (this.isFx) {
        var lin = script.absoluteLin(value, 0.0, 1.0);
        engine.setValue(this.effectRackName, "super1", lin);
    }
    else {
        var nonlin = script.absoluteNonLin(value, 0.0, 1.0, 4.0);
        engine.setValue(this.groupName, "filterMid", nonlin);
    }
};

/**
 * Button handler for the "toggle vinyl mode" button
 * 
 * Toggle vinyl mode. That's the "phono needle really doesn't look like this" button.
 */
Deck.prototype.handleVinyl = function(value) {
    if (value !== BUTTON_PRESSED) {
        return;
    }
    
    this.isVinylMode = ! this.isVinylMode;
    
    if (this.isVinylMode) {
       this.lights.vinyl.on();
    } else {
       this.lights.vinyl.off();
       engine.scratchDisable(this.deckNum);
    }
};

/**
 * You touched the wheel! Wheee!
 * 
 * @param value the control value
 */
Deck.prototype.handleWheelTouch = function(value) {
    if (! this.isVinylMode) {
        return;
    }
    
    if (value === BUTTON_PRESSED) // wheel touched?
    {
        // Ticks per revolution is 375. (or so I'm told.)
        engine.scratchEnable( this.deckNum, 
                              375,        // ticks per rev (approximate)
                              33.33333,   // rpm 
                              SCRATCH_ALPHA,
                              SCRATCH_BETA );
    }
    else { // Wheel untouched
        engine.scratchDisable(this.deckNum, SCRATCH_RAMP);
    }
};

/**
 * You TURNED the wheel?! ARE YOU MAD?!!
 * @param {number} value MIDI jog event value, the number of ticks, signed char
 */
Deck.prototype.handleJog = function(value) {
    // Direction = value = + or - 128 ticks
  
    if (engine.isScratching(this.deckNum)) {    // scratch
        engine.scratchTick(this.deckNum, value - 0x40);
    }
    else { // jog / pitchbend
        var offset = (value - 0x40) * 0.68;
        engine.setValue(this.groupName, "jog", offset);
    }
};

Deck.prototype.handleSyncUp = function(value) {
    engine.setValue(this.groupName, "rate_temp_up", value);
};

Deck.prototype.handleSyncDown = function(value) {
    engine.setValue(this.groupName, "rate_temp_down", value);
};

Deck.prototype.handleBeatjumpForward = function(value) {
    if (value) {
        engine.setValue(this.groupName, "beatjump_forward", 1);
    }
};

Deck.prototype.handleBeatjumpBackward = function(value) {
    if (value) {
        engine.setValue(this.groupName, "beatjump_backward", 1);
    }
};

//
// **** Construct the main global Spin entry point objects
//

Spin.deck1 = new Deck(0); // Deck 1 or A
Spin.deck2 = new Deck(1); // Deck 2 or B

/** @type {boolean} */
Spin.globalShift = false;

Spin.init = function(/* id */) {
    Spin.turnOffAllLights();

    if (MID_SOFT_TAKEOVER) {
        engine.softTakeover("[Channel1]", "filterMid", true);
        engine.softTakeover("[Channel2]", "filterMid", true);
    }
    if (FILTER_SOFT_TAKEOVER) {
        engine.softTakeover("[QuickEffectRack1_[Channel1]]", "super1", true);
        engine.softTakeover("[QuickEffectRack1_[Channel2]]", "super1", true);
    }
};

Spin.shutdown = function(/* id */) {
    Spin.turnOffAllLights();
};

Spin.turnOffAllLights = function() {
    for (var i = 0; i < DECK_LIGHTS.length; i++) {
        LightController.turnOff(0, DECK_LIGHTS[i]);
        LightController.turnOff(1, DECK_LIGHTS[i]);
    }
    
    for (i = 0; i < MISC_LIGHTS.length; i++) {
        LightController.turnOff(2, MISC_LIGHTS[i]);
    }
};

//
// **** Midi event handlers
//

/*
 * mixxx MIDI command event handler parameter quick reference
 *
 * @param channel MIDI channel (0x00 = Channel 1..0x0F = Channel 16,)
 * @param control Control/note number (byte 2)
 * @param value Value of the control (byte 3)
 * @param status MIDI status byte (byte 1)
 * @param group MixxxControl group (from the <group> value in the XML file)
 */

/**
 * Handle LOOP: IN/OUT button as a smart LOOP-OUT button
 */
Spin.handleLoopInOut = function(channel, control, value, status, group) {
    if (value !== BUTTON_PRESSED) {
        return;
    }
    
    // If loop is currently looping, treat loop-out as loop-disable
    
    var is_enabled = engine.getValue(group, "loop_enabled");
    if (is_enabled > 0.1) { // binary = 0 or 1?
        // "reloop_exit" is deprecated and replaced by "reloop_toggle" in mixxx 2.0.0.
        // But, "reloop_toggle" is not actually in mixxx 2.0.0? Or maybe the group name is special now.
        // Anyway, I can't find it.
        // Use deprecated version for now.
        engine.setValue(group, "reloop_exit", 1);
    }
    else {
        engine.setValue(group, "loop_out", 1);
    }
};

Spin.handleShift = function(channel, control, value) {
    // Could do something fancy but TINY CONTROLLER
    if (value) { // Shifted
        // Release any nonshift-only buttons
        Spin.deck1.handleSyncUp(0);
        Spin.deck1.handleSyncDown(0);
        Spin.deck2.handleSyncUp(0);
        Spin.deck2.handleSyncDown(0);
        
        Spin.globalShift = true;
    }
    else {
        // Release any shift-only functions
        Spin.globalShift = false;
    }
};

// Yes, these handlers could probably be magically generated
// using javascript magic.


Spin.handleFxDeck1 = function(channel, control, value) {
    Spin.deck1.handleFx(value);
};

Spin.handleFxDeck2 = function(channel, control, value) {
    Spin.deck2.handleFx(value);
};

Spin.handleMidDeck1 = function(channel, control, value) {
    Spin.deck1.handleMid(value);
};

Spin.handleMidDeck2 = function(channel, control, value) {
    Spin.deck2.handleMid(value);
};

Spin.handleVinylDeck1 = function(channel, control, value) {
    Spin.deck1.handleVinyl(value);
};

Spin.handleVinylDeck2 = function(channel, control, value) {
    Spin.deck2.handleVinyl(value);
};

Spin.handleWheelTouchDeck1 = function(channel, control, value) {
    Spin.deck1.handleWheelTouch(value);
};

Spin.handleWheelTouchDeck2 = function(channel, control, value) {
    Spin.deck2.handleWheelTouch(value);
};

Spin.handleJogDeck1 = function(channel, control, value) {
    Spin.deck1.handleJog(value);
};

Spin.handleJogDeck2 = function(channel, control, value) {
    Spin.deck2.handleJog(value);
};

Spin.handleSyncUpDeck1 = function(channel, control, value) {
    if (Spin.globalShift) {
        Spin.deck1.handleBeatjumpForward(value);
    }
    else {
        Spin.deck1.handleSyncUp(value);
    }
};

Spin.handleSyncDownDeck1 = function(channel, control, value) {
    if (Spin.globalShift) {
        Spin.deck1.handleBeatjumpBackward(value);
    }
    else {
        Spin.deck1.handleSyncDown(value);
    }
};

Spin.handleSyncUpDeck2 = function(channel, control, value) {
    if (Spin.globalShift) {
        Spin.deck2.handleBeatjumpForward(value);
    }
    else {
        Spin.deck2.handleSyncUp(value);
    }
};

Spin.handleSyncDownDeck2 = function(channel, control, value) {
    if (Spin.globalShift) {
        Spin.deck2.handleBeatjumpBackward(value);
    }
    else {
        Spin.deck2.handleSyncDown(value);
    }
};

}(Spin));
